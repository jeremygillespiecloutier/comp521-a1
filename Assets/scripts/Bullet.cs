﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    const float BulletSpeed = 20;
    float lifeTime = 0;
    public Vector3 direction;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        lifeTime += Time.deltaTime;
        this.transform.position += direction * Time.deltaTime * BulletSpeed;
        if (lifeTime >= 3)
            Destroy(this.gameObject);
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name!="Player")
        {
            Destroy(this.gameObject);
            if (collision.gameObject.GetComponent<Enemy>() != null)
            {
                int rand = Random.Range(0, 4);
                if (rand == 0)
                    Destroy(collision.gameObject);
            }
        }
    }
}
