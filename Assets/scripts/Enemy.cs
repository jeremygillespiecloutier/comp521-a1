﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public List<Cell> path=new List<Cell>();
    Vector3 start = Vector3.zero, stop = Vector3.zero;
    const float EnemySpeed = 2;
    int index = 0;
    float progress = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        progress += Time.deltaTime*EnemySpeed;
        Cell current = path[index];
        Vector3 pos = current.position + new Vector3(0, transform.localPosition.y, 0);
        if (progress>=1)
        {
            progress = 0;
            transform.localPosition = pos;
            index++;
            if (index >= path.Count)
            {
                progress = 1;
                index = 0;
            }
            current = path[index];
            start = transform.localPosition;
            stop = current.position + new Vector3(0, transform.localPosition.y, 0);
        }
        else
        {
            transform.localPosition = Vector3.Lerp(start, stop, progress);
        }
	}
}
