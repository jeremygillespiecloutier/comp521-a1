﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Grid : MonoBehaviour {

    public float snapX = 1, snapY=1, snapZ=1;

	// Use this for initialization
	void Start () {
		
	}
	// Update is called once per frame
	void Update ()
    {
        if (!Application.isPlaying)
        {
            float x = Mathf.Floor(transform.position.x / snapX) * snapX;
            float y = Mathf.Floor(transform.position.y / snapY) * snapY;
            float z = Mathf.Floor(transform.position.z / snapZ) * snapZ;
            transform.position = new Vector3(x, y, z);
        }
	}
}
