﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maze : MonoBehaviour {

    Cell[,]cells=new Cell[18, 18]; // The maze grid
    Cell entrance, exit;
    List<Room> rooms = new List<Room>(); // The maze rooms
    const float WallDimension = 3f; // The cell size scale factor
    const float startX = 4, endX = 9; // The entrance and exit positions

	void Start () {
        generateMaze();
        Transform crate = GameObject.Find("Crate").transform;
        float crateY = crate.position.y;
        crate.position = transform.position + exit.position + new Vector3(0, 0, WallDimension);
        crate.position = new Vector3(crate.position.x, crateY, crate.position.z);
    }

    void generateMaze()
    {
        rooms.Clear();
        for (int i = 0; i < cells.GetLength(0); i++)
        {
            for (int j = 0; j < cells.GetLength(1); j++)
            {
                Cell cell = new Cell();
                cell.i = i;
                cell.j = j;
                cells[i, j] = cell;
                cell.position = new Vector3(j * WallDimension, 0, i * WallDimension);
            }
        }
        entrance = cells[0, (int)Mathf.Round(startX)];
        exit = cells[cells.GetLength(0) - 1, (int)Mathf.Round(endX)];
        for (int i = 0; i < cells.GetLength(0); i++)
        {
            for (int j = 0; j < cells.GetLength(1); j++)
            {
                Cell cell = cells[i, j];
                if ((i - 1) >= 0)
                    cell.neighbors.Add(cells[i - 1, j]);
                if ((j - 1) >= 0)
                    cell.neighbors.Add(cells[i, j - 1]);
                if ((i + 1) < cells.GetLength(0))
                    cell.neighbors.Add(cells[i + 1, j]);
                if ((j + 1) < cells.GetLength(1))
                    cell.neighbors.Add(cells[i, j + 1]);
            }
        }
        for(int roomCount = 0; roomCount < 3; roomCount++)
        {
            List<Cell> choices = new List<Cell>();
            foreach(Cell cell in cells)
            {
                if(cell.i>0 && cell.i<cells.GetLength(0)-2 && cell.j>0 && cell.j < cells.GetLength(1) - 2)
                {
                    bool valid = true;
                    foreach (Room room in rooms)
                    {
                        foreach(Cell roomCell in room.cells)
                        {
                            float dist = Mathf.Abs(cell.i - roomCell.i) + Mathf.Abs(cell.j - roomCell.j);
                            if (dist < 4)
                                valid = false;
                        }
                    }
                    if (valid)
                        choices.Add(cell);
                }
            }
            Cell choice = choices[Random.Range(0, choices.Count)];
            createRoom(choice.i, choice.j);
        }
        Cell start = cells[Random.Range(0, cells.GetLength(0)), Random.Range(0, cells.GetLength(1))];
        List<Cell> visited = new List<Cell>();
        List<Cell> frontier = new List<Cell>();
        frontier.Add(start);
        while (frontier.Count > 0)
        {
            Cell current = frontier[Random.Range(0, frontier.Count)];
            frontier.Remove(current);
            visited.Add(current);
            List<Cell> predecessors = new List<Cell>();
            foreach (Cell other in current.neighbors)
            {
                if (!other.blocked)
                {
                    if (visited.Contains(other))
                        predecessors.Add(other);
                    else if (!frontier.Contains(other))
                        frontier.Add(other);
                }
            }
            if (predecessors.Count > 0)
            {
                Cell predecessor = predecessors[Random.Range(0, predecessors.Count)];
                current.connections.Add(predecessor);
                predecessor.connections.Add(current);
            }
        }
        List<Cell> enemyCells = new List<Cell>();
        foreach(Room room in rooms)
        {
            Cell entrance = room.cells[Random.Range(0, room.cells.Count)];
            List<Cell> links = new List<Cell>();
            foreach (Cell cell in entrance.neighbors)
                if (!entrance.connections.Contains(cell))
                    links.Add(cell);
            Cell link = links[Random.Range(0, links.Count)];
            entrance.connections.Add(link);
            link.connections.Add(entrance);
            enemyCells.Add(entrance);
        }
        foreach (Cell ec in enemyCells)
            createEnemy(ec);
        displayMaze();
    }

    void createRoom(int coord1, int coord2)
    {
        Room room= new Room();
        rooms.Add(room);
        room.cells.Add(cells[coord1, coord2]);
        room.cells.Add(cells[coord1 + 1, coord2]);
        room.cells.Add(cells[coord1, coord2 + 1]);
        room.cells.Add(cells[coord1 + 1, coord2 + 1]);

        for (int i = 0; i < room.cells.Count; i++)
        {
            Cell current = room.cells[i];
            current.blocked = true;
            for (int j = 0; j < room.cells.Count; j++)
            {
                Cell other = room.cells[j];
                if (current != other && current.neighbors.Contains(other))
                    current.connections.Add(other);
            }
        }
        GameObject key=Instantiate(Resources.Load("Key")) as GameObject;
        key.transform.parent = transform;
        key.transform.localPosition = new Vector3((coord2 + 0.5f) * WallDimension, 2, (coord1 + 0.5f) * WallDimension);
        key.transform.localScale = new Vector3(1, 1, 1) * 0.1f;
    }

    void displayMaze()
    {
        for (int i = 0; i < cells.GetLength(0); i++)
        {
            for (int j = 0; j < cells.GetLength(1); j++)
            {
                Cell cell = cells[i, j];
                if (i< cells.GetLength(0)-1 && !cell.connections.Contains(cells[i + 1, j]))
                    addWall(j, i+0.5f, 1, 0.05f);
                if (j< cells.GetLength(1)-1 && !cell.connections.Contains(cells[i, j+1]))
                    addWall(j+0.5f, i, 0.05f, 1);
            }
        }
        float minX = -0.5f;
        float maxX = cells.GetLength(1)- 0.5f;
        float minZ = -0.5f;
        float maxZ = cells.GetLength(0) - 0.5f;
        addWall(minX, (minZ + maxZ) / 2, 0.05f, maxZ - minZ);
        addWall(maxX, (minZ + maxZ) / 2, 0.05f, maxZ - minZ);
        addWall((minX + startX - 0.5f) / 2, minZ, startX - 0.5f - minX, 0.05f);
        addWall((startX+0.5f+maxX)/2, minZ, maxX-startX-0.5f, 0.05f);
        addWall((minX + endX - 0.5f) / 2, maxZ, endX - 0.5f - minX, 0.05f);
        addWall((endX + 0.5f + maxX) / 2, maxZ, maxX - endX - 0.5f, 0.05f);
    }

    void addWall(float x, float z, float scaleX, float scaleZ)
    {
        GameObject wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wall.transform.parent = transform;
        wall.transform.localPosition = new Vector3(x*WallDimension, 2, z*WallDimension);
        wall.transform.localScale = new Vector3(scaleX*WallDimension, 4, scaleZ*WallDimension);
        Rigidbody rigidbody=wall.AddComponent<Rigidbody>();
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.R))
        {
            for(int i = 0; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
            generateMaze();
        }
	}
    void createEnemy(Cell pos)
    {
        GameObject enemy = Instantiate(Resources.Load("Enemy")) as GameObject;
        enemy.transform.parent = transform;
        enemy.transform.localPosition = new Vector3(pos.j, 1, pos.i) * WallDimension;
        path.Clear();
        foreach (Cell cell in cells)
            cell.visited = false;
        DFS(pos);
        foreach (Cell cell in path)
            enemy.GetComponent<Enemy>().path.Add(cell);
    }
    void DFS(Cell cell)
    {
        cell.visited = true;
        path.Add(cell);
        List<Cell> connections = new List<Cell>();
        connections.AddRange(cell.connections);
        while (connections.Count > 0)
        {
            int index = Random.Range(0, connections.Count);
            Cell other = connections[index];
            connections.RemoveAt(index);
            if (!other.visited)
            {
                DFS(other);
                path.Add(cell);
            }
        }
        return;
    }
    List<Cell> path=new List<Cell>();
}

public class Cell
{
    public List<Cell> neighbors = new List<Cell>();
    public List<Cell> connections=new List<Cell>();
    public int i, j;
    public Cell from;
    public bool blocked, visited;
    public Vector3 position;
}

class Room
{
    public List<Cell> cells = new List<Cell>();
}
