﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    const float CamSensitivity = 3;
    const float PlayerSpeed = 10;
    float cameraX=0, cameraY=0;
    GameObject bullet = null;
    int keys = 0;
    bool won = false;

	// Use this for initialization
	void Start () {
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update () {
        if (won)
        {
            if (Input.GetMouseButtonDown(0))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            return;
        }
        cameraX += Input.GetAxis("Mouse X")*CamSensitivity;
        if (cameraX >= 360)
            cameraX -= 360;
        if (cameraX <= -360)
            cameraX += 360;
        cameraY -= Input.GetAxis("Mouse Y")*CamSensitivity;
        cameraY = Mathf.Clamp(cameraY, -70, 70);
        Camera.main.transform.position = transform.position+new Vector3(0, 1, 0);
        Camera.main.transform.rotation = Quaternion.Euler(cameraY, cameraX, 0);
        Vector3 movDirec = new Vector3();
        if(Input.GetKey(KeyCode.W))
            movDirec += new Vector3(0, 0, 1);
        if (Input.GetKey(KeyCode.S))
            movDirec -= new Vector3(0, 0, 1);
        if (Input.GetKey(KeyCode.A))
            movDirec -= new Vector3(1, 0, 0);
        if (Input.GetKey(KeyCode.D))
            movDirec += new Vector3(1, 0, 0);
        if (movDirec.sqrMagnitude>0)
        {
            movDirec.Normalize();
            movDirec = Quaternion.Euler(0, cameraX, 0) * movDirec;
        }
        movDirec += new Vector3(0, -PlayerSpeed, 0);
        GetComponent<CharacterController>().SimpleMove(movDirec * PlayerSpeed);
        // To remove
        if (Input.GetKey(KeyCode.E))
            transform.position += new Vector3(0, PlayerSpeed * Time.deltaTime, 0);
        if (Input.GetKey(KeyCode.Q))
            transform.position += new Vector3(0, -PlayerSpeed * Time.deltaTime, 0);
        if (Input.GetKeyDown(KeyCode.Space) && bullet==null)
        {
            bullet = Instantiate(Resources.Load("Bullet")) as GameObject;
            bullet.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0.3f));
            bullet.GetComponent<Bullet>().direction=Camera.main.transform.forward;
        }
    }
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject == GameObject.Find("Trapdoor"))
        {
            Destroy(hit.gameObject);
        }else if (hit.gameObject.name == "KeyMesh")
        {
            Destroy(hit.gameObject.transform.parent.gameObject);
            keys++;
            GameObject.Find("KeyLabel").GetComponent<Text>().text = keys + " key" + (keys == 1 ? "" : "s");
            if (keys == 3)
                Destroy(GameObject.Find("Crate"));
        }else if (hit.gameObject.name == "ChestMesh")
        {
            won = true;
            GameObject.Find("WinText").GetComponent<Text>().enabled = true;
        }

    }
}
